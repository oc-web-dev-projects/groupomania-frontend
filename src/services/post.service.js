import axios from 'axios'

axios.defaults.withCredentials = true

const url = 'http://localhost:3000/api/posts/'

function createPost (data) {
  return axios({
    method: 'post',
    url: url,
    data: data,
    headers: { 'Content-Type': 'multipart/form-data' }
  })
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function getPostById (id) {
  return axios.get(url + id)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function getAllPosts () {
  return axios.get(url)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function updatePost (id, data) {
  return axios({
    method: 'put',
    url: url + id,
    data: data,
    headers: { 'Content-Type': 'multipart/form-data' }
  })
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function deletePost (id) {
  return axios.delete(url + id)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

export const postService = {
  createPost,
  getPostById,
  getAllPosts,
  updatePost,
  deletePost
}
