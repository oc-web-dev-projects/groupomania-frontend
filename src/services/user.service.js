import axios from 'axios'

import router from '../router/index'
axios.defaults.withCredentials = true

const url = 'http://localhost:3000/api/users/'

function signup (data) {
  return axios.post(url + 'signup', data)
    .then((res) => { router.push('/login') })
    .catch((error) => {
      throw error
    })
}

function login (data) {
  return axios.post(url + 'login', data)
    .then((res) => { return res })
    .catch((error) => {
      throw error
    })
}

function getUserById (id) {
  return axios.get(url + id)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function getAllUsers () {
  return axios.get(url)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function updateUser (id, data) {
  return axios({
    method: 'put',
    url: url + id,
    data: data,
    headers: { 'Content-Type': 'multipart/form-data' }
  })
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function deleteUser (id) {
  return axios.delete(url + id)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

export const userService = {
  signup,
  login,
  getUserById,
  getAllUsers,
  updateUser,
  deleteUser
}
