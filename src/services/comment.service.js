import axios from 'axios'

axios.defaults.withCredentials = true

const url = 'http://localhost:3000/api/posts/'
const path = '/comments/'

function createComment (postId, data) {
  return axios.post(url + postId + path, data)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function getCommentById (postId, commentId) {
  return axios.get(url + postId + path + commentId)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function getAllComments (postId) {
  return axios.get(url + postId + path)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function updateComment (postId, commentId, data) {
  return axios.put(url + postId + path + commentId, data)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

function deleteComment (postId, commentId) {
  return axios.delete(url + postId + path + commentId)
    .then((res) => { return res.data })
    .catch((error) => {
      throw error
    })
}

export const commentService = {
  createComment,
  getCommentById,
  getAllComments,
  updateComment,
  deleteComment
}
