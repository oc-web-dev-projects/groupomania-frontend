import { userService } from '../services/user.service'
import router from '../router/index'

const state = {
  loggedIn: false,
  currentUser: null
}

const mutations = {
  loginSuccess (state, user) {
    state.loggedIn = true
    state.currentUser = user
  },
  loginFailure (state, error) {
    state.loggedIn = false
    state.currentUser = null
  },
  logout (state) {
    state.loggedIn = false
    state.currentUser = null
  }
}

const actions = {
  login ({ dispatch, commit }, data) {
    userService.login(data)
      .then(
        data => {
          const user = data.data.user
          commit('loginSuccess', user)
          router.push('/wall')
        },
        error => {
          commit('loginFailure', error)
          dispatch('alert/error', error, { root: true })
        }
      )
  }
}

export const auth = {
  namespaced: true,
  state,
  actions,
  mutations
}
