import { postService } from '../services/post.service'

const state = {
  allPosts: []
}

const mutations = {
  getPostsSuccess (state, posts) {
    state.allPosts = posts
    console.log(state.allPosts)
  },
  getPostsFailure (state, error) {
    state.posts = []
  }
}

const actions = {
  getPosts ({ dispatch, commit }) {
    postService.getAllPosts()
      .then(
        data => {
          const posts = data
          commit('getPostsSuccess', posts)
        },
        error => {
          commit('getPostsFailure', error)
          dispatch('alert/error', error, { root: true })
        }
      )
  }
}

export const post = {
  namespaced: true,
  state,
  actions,
  mutations
}
